import json
import random
from functools import reduce

import requests
from django.http import JsonResponse
from django.shortcuts import render
from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from api.models import Chiste
from api.serializers import (ChisteSerializer, ConsecutivoSerializer,
                             McmSerializer)


class ChisteViewSet(ModelViewSet):
    queryset = Chiste.objects.all()
    serializer_class = ChisteSerializer

    def list(self, request):
        param = request.GET.get("param")
        if param:
            if "Chuck" in param:
                res = requests.get(
                    "https://api.chucknorris.io/jokes/random",
                    headers={"content_type": "application/json"},
                )
                resultado = json.loads(json.dumps(res.json()))
                data = {"param": param, "chiste": resultado["value"]}
                return JsonResponse(data, status=200)
            elif "Dad" in param:
                res = requests.get(
                    "https://icanhazdadjoke.com/",
                    headers={"Accept": "application/json"},
                )
                resultado = json.loads(json.dumps(res.json()))
                data = {"param": param, "chiste": resultado["joke"]}
                return JsonResponse(data, status=200)
            else:
                data = {
                    "msg": "Error en la busqueda con el valor param",
                }
                return JsonResponse(data, status=400)
        else:
            aleatorio = random.randint(1, 2)
            if aleatorio == 1:
                res = requests.get(
                    "https://api.chucknorris.io/jokes/random",
                    headers={"content_type": "application/json"},
                )
                resultado = json.loads(json.dumps(res.json()))
                data = {"param": param, "chiste": resultado["value"]}
                return JsonResponse(data, status=200)
            else:
                res = requests.get(
                    "https://icanhazdadjoke.com/",
                    headers={"Accept": "application/json"},
                )
                resultado = json.loads(json.dumps(res.json()))
                data = {"param": param, "chiste": resultado["joke"]}
                return JsonResponse(data, status=200)


class McmApi(ListAPIView):
    serializer_class = McmSerializer

    def list(self, request):
        cadena = request.GET.get("lista")
        if cadena:
            try:
                lista = [int(x) for x in cadena.split(",")]
                total = reduce((lambda n, m: n * m), lista)
                menor = min(lista)
                for i in range(1, menor):
                    ingresa = True
                    for x in lista:
                        if not x % i == 0:
                            ingresa = False
                    if ingresa:
                        mcm = total / i
                data = {"mcm": mcm}
                return JsonResponse(data, status=200)
            except (ValueError, TypeError) as e:
                data = {
                    "msg": "Error la lista no contiene numeros enteros",
                }
                return JsonResponse(data, status=400)
        else:
            data = {
                "msg": "Error la lista no existe",
            }
            return JsonResponse(data, status=400)


class ConsecutivoApi(ListAPIView):
    serializer_class = ConsecutivoSerializer

    def list(self, request):
        entero = request.GET.get("entero")
        if entero:
            try:
                data = {"entero": int(entero) + 1}
                return JsonResponse(data, status=200)
            except (ValueError, TypeError) as e:
                data = {
                    "msg": "Error el valor no es entero",
                }
                return JsonResponse(data, status=400)
        else:
            data = {
                "msg": "Error el entero no existe",
            }
            return JsonResponse(data, status=400)
