from rest_framework import serializers

from api.models import Chiste


class ChisteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Chiste
        fields = ("id", "param", "chiste")


class McmSerializer(serializers.Serializer):
    lista = serializers.CharField()


class ConsecutivoSerializer(serializers.Serializer):
    entero = serializers.IntegerField()
