from django.db import models


class Chiste(models.Model):
    param = models.CharField(max_length=100)
    chiste = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.chiste
