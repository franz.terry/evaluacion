from django.test import TestCase


class TestListChiste(TestCase):
    def setUp(self) -> None:
        self.base_url = "/app/chistes/"

    def test_list_param(self):
        data = {"param": "Dad"}
        response = self.client.get(
            self.base_url, data=data, content_type="application/json"
        )
        assert response.status_code == 200

    def test_list_sin_param(self):
        data = {}
        response = self.client.get(
            self.base_url, data=data, content_type="application/json"
        )
        assert response.status_code == 200


class TestCreateChiste(TestCase):
    def setUp(self) -> None:
        self.base_url = "/app/chistes/"

    def test_create_chiste(self):
        data = {"param": "Ded", "chiste": "Chiste de prueba"}

        response = self.client.post(
            self.base_url, data=data, content_type="application/json"
        )
        assert response.status_code == 201


class TestEntero(TestCase):
    def setUp(self) -> None:
        self.base_url = "/app/consecutivo/"

    def test_entero(self):
        data = {"entero": "5"}
        response = self.client.get(
            self.base_url, data=data, content_type="application/json"
        )
        assert response.status_code == 200


class TestMcm(TestCase):
    def setUp(self) -> None:
        self.base_url = "/app/mcm/"

    def test_entero1(self):
        data = {"lista": "12,18"}
        response = self.client.get(
            self.base_url, data=data, content_type="application/json"
        )
        assert response.status_code == 200

    def test_entero2(self):
        data = {"lista": "12,18,30"}
        response = self.client.get(
            self.base_url, data=data, content_type="application/json"
        )
        assert response.status_code == 200
