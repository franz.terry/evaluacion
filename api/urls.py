from django.urls import re_path
from rest_framework import routers

from api.views import ChisteViewSet, ConsecutivoApi, McmApi

app_name = "app"

router = routers.SimpleRouter()
router.register(r"chistes", ChisteViewSet)

urlpatterns = [
    re_path(r"^mcm/$", McmApi.as_view(), name="mcm"),
    re_path(r"^consecutivo/", ConsecutivoApi.as_view(), name="consecutivo"),
] + router.urls
