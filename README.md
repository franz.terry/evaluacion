# Proyecto de Evaluacion
 - Es un proyecto de evaluacion
 - Basado en:
    - Python
    - Django
    - Base de datos en postgres

## Desarrolladores/Programadores

- Si se quiere agregar/cambiar/quitar dependencias, realizar los cambios en los archivos `dev.in`, `linter.in` o `test.in` según sea necesario.
-- **NO modificar manualmente los archivos `.txt`** de requerimientos.
-- Una vez que se haya hecho el cambio en el archivo `.in` correspondiente, se deben "compilar" los archivos a `.txt`, para eso entrar a la carpeta `requirements` y ejecutar el comando:
    ```
    cd requirements/
    ./compile_reqs.sh

- Para instalar/actualizar los requerimientos del proyecto: framework y librerias:
    ```
    pip install -r requirements/test.txt
    ```

- Luego proceder a cargar las migraciones:
    ```
    python manage.py migrate

- Cargar los fixtures en el siguiente orden:
    ejecutar el archivo `./load_fixtures.sh` (agregar los comandos de carga cuando sean necesarios en ese archivo también)